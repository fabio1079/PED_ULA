----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.07.2018 21:01:50
-- Design Name: 
-- Module Name: error_decoder_to_7seg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity error_decoder_to_7seg is
    Port ( seg_0 : out STD_LOGIC_VECTOR(6 downto 0);
           seg_1 : out STD_LOGIC_VECTOR(6 downto 0);
           seg_2 : out STD_LOGIC_VECTOR(6 downto 0);
           seg_3 : out STD_LOGIC_VECTOR(6 downto 0));
end error_decoder_to_7seg;

architecture Behavioral of error_decoder_to_7seg is

begin

    -- ERRO --> seg_3, seg_2, seg_1, seg_0

    -- Anodo comum (acende em '0')
    seg_0 <= "1000000"; -- o
    seg_1 <= "1001110"; -- R --> e, f, a
    seg_2 <= "1001110"; -- R
    seg_3 <= "0000110"; -- E

end Behavioral;
