----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.07.2018 22:48:39
-- Design Name: 
-- Module Name: complement_of_1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity complement_of_1 is
    Port ( A : in STD_LOGIC_VECTOR(3 downto 0);
           S : out STD_LOGIC_VECTOR(3 downto 0));
end complement_of_1;

architecture Behavioral of complement_of_1 is

begin
    S(0) <= not A(0);
    S(1) <= not A(1);
    S(2) <= not A(2);
    S(3) <= not A(3);
end Behavioral;
