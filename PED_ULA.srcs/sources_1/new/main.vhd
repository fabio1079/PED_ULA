----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.07.2018 22:15:47
-- Design Name: 
-- Module Name: main - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
    Port ( A : in STD_LOGIC_VECTOR(3 downto 0);
           B : in STD_LOGIC_VECTOR(3 downto 0);
           S : in STD_LOGIC_VECTOR(3 downto 0); -- command
           DisplayMode : in STD_LOGIC_VECTOR(1 downto 0); -- 00 dec, 01 hex, 10 bin,  11 oct
           OperationMode : in STD_LOGIC; -- 0 CALC. 1 PROG
           clk: in STD_LOGIC;

           an: out STD_LOGIC_VECTOR (3 downto 0) := "1111"; --anodos dos displays
           seg : out STD_LOGIC_VECTOR(6 downto 0) -- display 7 seg
    );
end main;

architecture Behavioral of main is

    component calculator is
        Port ( A : in STD_LOGIC_VECTOR(3 downto 0);
               B : in STD_LOGIC_VECTOR(3 downto 0);
               S : in STD_LOGIC_VECTOR(3 downto 0);
               F : out STD_LOGIC_VECTOR(3 downto 0);
               Over : out STD_LOGIC);
    end component;

    component base_16_to_7seg is
        Port ( base_16 : in STD_LOGIC_VECTOR(3 downto 0);
               seg : out STD_LOGIC_VECTOR(6 downto 0));
    end component;

    component base_10_to_7seg is
        Port ( base_10 : in STD_LOGIC_VECTOR(3 downto 0);
               seg : out STD_LOGIC_VECTOR(6 downto 0));
    end component;

    component base_8_to_7seg is
        Port ( base_8 : in STD_LOGIC_VECTOR(2 downto 0);
               seg : out STD_LOGIC_VECTOR(6 downto 0));
    end component;

    component base_2_to_7seg is
        Port ( bcd : in STD_LOGIC_VECTOR(3 downto 0);
               seg_0 : out STD_LOGIC_VECTOR(6 downto 0);
               seg_1 : out STD_LOGIC_VECTOR(6 downto 0);
               seg_2 : out STD_LOGIC_VECTOR(6 downto 0);
               seg_3 : out STD_LOGIC_VECTOR(6 downto 0));
    end component;

    component error_decoder_to_7seg is
        Port ( seg_0 : out STD_LOGIC_VECTOR(6 downto 0);
               seg_1 : out STD_LOGIC_VECTOR(6 downto 0);
               seg_2 : out STD_LOGIC_VECTOR(6 downto 0);
               seg_3 : out STD_LOGIC_VECTOR(6 downto 0));
    end component;

    signal clk_divided: STD_LOGIC:= '0';
    signal clk_count: STD_LOGIC_VECTOR (15 downto 0) := "0000000000000000";
    signal sig_calc:  STD_LOGIC_VECTOR(3 downto 0) := "0000";
    signal sig_overflow: STD_LOGIC := '0';
    signal bcd_display: STD_LOGIC_VECTOR(6 downto 0); -- display 7 seg

    signal err_display_0 : STD_LOGIC_VECTOR(6 downto 0); -- O
    signal err_display_1 : STD_LOGIC_VECTOR(6 downto 0); -- R
    signal err_display_2 : STD_LOGIC_VECTOR(6 downto 0); -- R
    signal err_display_3 : STD_LOGIC_VECTOR(6 downto 0); -- E

    signal sig_base_2_display_0 : STD_LOGIC_VECTOR(6 downto 0);
    signal sig_base_2_display_1 : STD_LOGIC_VECTOR(6 downto 0);
    signal sig_base_2_display_2 : STD_LOGIC_VECTOR(6 downto 0);
    signal sig_base_2_display_3 : STD_LOGIC_VECTOR(6 downto 0);
    signal sig_counter_disp : STD_LOGIC_VECTOR(1 downto 0) := "00";

    signal sig_base_8_display : STD_LOGIC_VECTOR(6 downto 0);
    signal sig_base_10_display : STD_LOGIC_VECTOR(6 downto 0);
    signal sig_base_16_display : STD_LOGIC_VECTOR(6 downto 0);
begin

    error_display_signals: error_decoder_to_7seg port map(
        seg_0 => err_display_0,
        seg_1 => err_display_1,
        seg_2 => err_display_2,
        seg_3 => err_display_3);
        
    execute_calc: calculator port map (
        A => A,
        B => B,
        S => S,
        F => sig_calc,
        Over => sig_overflow);

    base_2_disp: base_2_to_7seg port map(
        bcd => sig_calc,
        seg_0 => sig_base_2_display_0,
        seg_1 => sig_base_2_display_1,
        seg_2 => sig_base_2_display_2,
        seg_3 => sig_base_2_display_3);

    base_8_disp: base_8_to_7seg port map(
        base_8 => sig_calc(2 downto 0),
        seg => sig_base_8_display);

    base_10_disp: base_10_to_7seg port map(
        base_10 => sig_calc,
        seg => sig_base_10_display);

    base_16_disp: base_16_to_7seg port map(
        base_16 => sig_calc,
        seg => sig_base_16_display);

    -- Clock Divider
    clock_divider: process(clk)
    begin
        if rising_edge(clk) then
            clk_count <= clk_count + 1;
        end if;

        clk_divided <= clk_count(15);
    end process;

    -- inc display an counter: 00 -> 01 -> 10 -> 11 -> 00
    counter_display: process(clk_divided)
    begin
        sig_counter_disp <= sig_counter_disp + 1;
    end process;

    -- display process
    display_on_clk: process(clk_divided)
    begin
        -- display erro on calc overflow
        if sig_overflow = '1' then
            if sig_counter_disp = "00" then
                an <= "1110";
                bcd_display <= err_display_0; -- O
            elsif sig_counter_disp = "01" then
                an <= "1101";
                bcd_display <= err_display_1; -- R
            elsif sig_counter_disp = "10" then
                an <= "1011";
                bcd_display <= err_display_2; -- R
            else -- 11
                an <= "0111";
                bcd_display <= err_display_3; -- E
            end if; 
            
        else -- when there is no overflow, display calc
            case DisplayMode is
                when "00" =>
                    an <= "1110";
                    bcd_display <= sig_base_10_display;

                when "01" =>
                    an <= "1110";
                    bcd_display <= sig_base_16_display;

                when "10" =>
                    if sig_counter_disp = "00" then
                        an <= "1110";
                        bcd_display <= sig_base_2_display_0;
                    elsif sig_counter_disp = "01" then
                        an <= "1110";
                        bcd_display <= sig_base_2_display_1;
                    elsif sig_counter_disp = "10" then
                        an <= "1110";
                        bcd_display <= sig_base_2_display_2;
                    else -- 11
                        an <= "1110";
                        bcd_display <= sig_base_2_display_3;
                    end if;

                when "11" =>
                    an <= "1110";
                    bcd_display <= sig_base_8_display;

            end case;

        end if;
    end process;
    
    seg <= bcd_display;

end Behavioral;
