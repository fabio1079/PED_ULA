----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.07.2018 22:39:30
-- Design Name: 
-- Module Name: complement_of_2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity complement_of_2 is
    Port ( A : in STD_LOGIC_VECTOR(3 downto 0);
           S : out STD_LOGIC_VECTOR(3 downto 0);
           Cout : out STD_LOGIC);
end complement_of_2;

architecture Behavioral of complement_of_2 is

    component complement_of_1
        port(
            A : in STD_LOGIC_VECTOR(3 downto 0);
            S : out STD_LOGIC_VECTOR(3 downto 0)
        );
    end component;

    signal A_comp_1 : STD_LOGIC_VECTOR(3 downto 0);
    signal temp : STD_LOGIC_VECTOR(4 downto 0) := "00000";

begin

    C1: complement_of_1 port map(A => A, S => A_comp_1);
    
    C2: process(A_comp_1, temp) is
    begin
        temp <= std_logic_vector(unsigned('0' & A_comp_1) + 1);
        S <= temp(3 downto 0);
        Cout <= temp(4);
    end process; 
       
end Behavioral;
