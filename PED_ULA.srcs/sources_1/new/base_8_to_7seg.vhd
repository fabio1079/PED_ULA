----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.07.2018 20:03:12
-- Design Name: 
-- Module Name: base_8_to_7seg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity base_8_to_7seg is
    Port ( base_8 : in STD_LOGIC_VECTOR(2 downto 0);
           seg : out STD_LOGIC_VECTOR(6 downto 0));
end base_8_to_7seg;

architecture Behavioral of base_8_to_7seg is

begin

    with base_8 select   
        --Anodo comum (acende em '0')
        seg <= "1000000" when "000",  -- '0'
               "1111001" when "001",  -- '1'
               "0100100" when "010",  -- '2'
               "0110000" when "011",  -- '3'
               "0011001" when "100",  -- '4' 
               "0010010" when "101",  -- '5'
               "0000010" when "110",  -- '6'
               "1111000" when "111",  -- '7'
               "1111111" when others;  -- clear

end Behavioral;
