----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05.07.2018 21:35:19
-- Design Name: 
-- Module Name: base_2_to_7seg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity base_2_to_7seg is
    Port ( bcd : in STD_LOGIC_VECTOR(3 downto 0);
           seg_0 : out STD_LOGIC_VECTOR(6 downto 0);
           seg_1 : out STD_LOGIC_VECTOR(6 downto 0);
           seg_2 : out STD_LOGIC_VECTOR(6 downto 0);
           seg_3 : out STD_LOGIC_VECTOR(6 downto 0));
end base_2_to_7seg;

architecture Behavioral of base_2_to_7seg is

    signal sig_seg_0 : STD_LOGIC_VECTOR(6 downto 0);
    signal sig_seg_1 : STD_LOGIC_VECTOR(6 downto 0);
    signal sig_seg_2 : STD_LOGIC_VECTOR(6 downto 0);
    signal sig_seg_3 : STD_LOGIC_VECTOR(6 downto 0);

begin

    seg_0 <= sig_seg_0;
    seg_1 <= sig_seg_1;
    seg_2 <= sig_seg_2;
    seg_3 <= sig_seg_3;

    check_bcd: process(bcd)
    begin
        case bcd is
            when "0000" =>
                sig_seg_0 <= "1000000";
                sig_seg_1 <= "1000000";
                sig_seg_2 <= "1000000";
                sig_seg_3 <= "1000000";
                
            when "0001" =>
                sig_seg_0 <= "1111001";
                sig_seg_1 <= "1000000";
                sig_seg_2 <= "1000000";
                sig_seg_3 <= "1000000";
                        
            when "0010" =>
                sig_seg_0 <= "1000000";
                sig_seg_1 <= "1111001";
                sig_seg_2 <= "1000000";
                sig_seg_3 <= "1000000";
                
            when "0011" =>
                sig_seg_0 <= "1111001";
                sig_seg_1 <= "1111001";
                sig_seg_2 <= "1000000";
                sig_seg_3 <= "1000000";
                        
            when "0100" =>
                sig_seg_0 <= "1000000";
                sig_seg_1 <= "1000000";
                sig_seg_2 <= "1111001";
                sig_seg_3 <= "1000000";
                        
            when "0101" =>
                sig_seg_0 <= "1111001";
                sig_seg_1 <= "1000000";
                sig_seg_2 <= "1111001";
                sig_seg_3 <= "1000000";
                
            when "0110" =>
                sig_seg_0 <= "1000000";
                sig_seg_1 <= "1111001";
                sig_seg_2 <= "1111001";
                sig_seg_3 <= "1000000";
                
            when "0111" =>
                sig_seg_0 <= "1111001";
                sig_seg_1 <= "1111001";
                sig_seg_2 <= "1111001";
                sig_seg_3 <= "1000000";
                
            when "1000" =>
                sig_seg_0 <= "1000000";
                sig_seg_1 <= "1000000";
                sig_seg_2 <= "1000000";
                sig_seg_3 <= "1111001";
                
            when "1001" =>
                sig_seg_0 <= "1111001";
                sig_seg_1 <= "1000000";
                sig_seg_2 <= "1000000";
                sig_seg_3 <= "1111001";
                
            when "1010" =>
                sig_seg_0 <= "1000000";
                sig_seg_1 <= "1111001";
                sig_seg_2 <= "1000000";
                sig_seg_3 <= "1111001";
                
            when "1011" =>
                sig_seg_0 <= "1111001";
                sig_seg_1 <= "1111001";
                sig_seg_2 <= "1000000";
                sig_seg_3 <= "1111001";
                
            when "1100" =>
                sig_seg_0 <= "1000000";
                sig_seg_1 <= "1000000";
                sig_seg_2 <= "1111001";
                sig_seg_3 <= "1111001";
                
            when "1101" =>
                sig_seg_0 <= "1111001";
                sig_seg_1 <= "1000000";
                sig_seg_2 <= "1111001";
                sig_seg_3 <= "1111001";
                
            when "1110" =>
                sig_seg_0 <= "1000000";
                sig_seg_1 <= "1111001";
                sig_seg_2 <= "1111001";
                sig_seg_3 <= "1111001";
                
            when "1111" =>
                sig_seg_0 <= "1111001";
                sig_seg_1 <= "1111001";
                sig_seg_2 <= "1111001";
                sig_seg_3 <= "1111001";
                
        end case;
    end process;

end Behavioral;

