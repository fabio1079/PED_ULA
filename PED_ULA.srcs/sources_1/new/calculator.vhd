----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.07.2018 22:31:17
-- Design Name: 
-- Module Name: calculator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity calculator is
    Port ( A : in STD_LOGIC_VECTOR(3 downto 0);
           B : in STD_LOGIC_VECTOR(3 downto 0);
           S : in STD_LOGIC_VECTOR(3 downto 0);
           F : out STD_LOGIC_VECTOR(3 downto 0);
           Over : out STD_LOGIC);
end calculator;

architecture Behavioral of calculator is

    component complement_of_2
        port(
            A : in STD_LOGIC_VECTOR(3 downto 0);
            S : out STD_LOGIC_VECTOR(3 downto 0);
            Cout : out STD_LOGIC
        );
    end component;

    signal calc : STD_LOGIC_VECTOR(4 downto 0) := "00000";
    
    signal A_comp_2 : STD_LOGIC_VECTOR(3 downto 0);
    signal sOver : STD_LOGIC := '0';

begin

    C2:complement_of_2 port map(A => A, S => A_comp_2, Cout => sOver);

    process(A, B, S, calc, sOver) is
    begin
        case S is
            when "0000" => -- ZERA 0000
                F <= "0000";
            
            when "0001" => -- TUDOUM 1111
                F <= "1111";
            
            when "0010" => -- OPA A0A1A2A
                F <= A;
            
            when "0011" => -- OPB B0B1B2B3
                F <= B;
            
            when "0100" => -- OR(A,B) OR bit a bit 
                for i in 3 downto 0 loop
                    F(i) <= A(i) or B(i);
                end loop;
                
            when "0101" => -- AND(A,B) AND bit a bit
                for i in 3 downto 0 loop
                    F(i) <= A(i) and B(i);
                end loop;
            
            when "0110" => -- XOR(A,B) XOR bit a bit
                for i in 3 downto 0 loop
                    F(i) <= A(i) xor B(i);
                end loop;
                            
            when "0111" => -- NOTA NOT bit a bit 
                for i in 3 downto 0 loop
                    F(i) <= not A(i);
                end loop;
                
            when "1000" => -- SOMA(A,B)  A+B 
                -- https://stackoverflow.com/questions/4042832/error-adding-std-logic-vectors
                -- https://stackoverflow.com/questions/30870834/how-to-add-two-16-bit-std-logic-vector-and-a-carry-into-17-bit-in-vhdl
                
                calc <= std_logic_vector(unsigned('0' & A) + unsigned('0' & B));
                F <= calc(3 downto 0); -- calc has size 4, so copy only from 3 down 0 to F
                Over <= calc(4); -- the last calc position will be the overflow
                
            when "1001" => -- SUBT(A,B) A-B 
                F <= std_logic_vector(unsigned(A) - unsigned(B));
                
            when "1010" => -- MULT(A,B) A*B
                calc <= std_logic_vector(resize(unsigned("0" & A) * unsigned('0' & B), 5));
                F <= calc(3 downto 0);
                Over <= calc(4);
                
            when "1011" => -- DIV(A,B) A/B 
                calc <= std_logic_vector(unsigned('0' & A) / unsigned('0' & B));
                F <= calc(3 downto 0);
                Over <= calc(4);
                            
            when "1100" => -- MOD(A,B) A mod B
                F <= std_logic_vector(unsigned(A) mod unsigned(B));
                
            when "1101" => -- SQA(A) A^2
                calc <= std_logic_vector(resize(unsigned('0' & A) * unsigned('0' & A), 5));
                F <= calc(3 downto 0);
                Over <= calc(4);

            when "1110" => -- NEGA -A
                F <= A_comp_2;
                Over <= sOver;
                
            when "1111" => -- ADD1 A+1
                calc <= std_logic_vector(unsigned('0' & A) + 1);
                F <= calc(3 downto 0);
                Over <= calc(4);
        end case;
    end process;

end Behavioral;
