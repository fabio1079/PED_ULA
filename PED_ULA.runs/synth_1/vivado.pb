
s
Command: %s
53*	vivadotcl2B
.synth_design -top main -part xc7a35ticpg236-1L2default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7a35ti2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7a35ti2default:defaultZ17-349h px� 
�
%s*synth2�
�Starting RTL Elaboration : Time (s): cpu = 00:00:16 ; elapsed = 00:00:17 . Memory (MB): peak = 1248.301 ; gain = 73.988 ; free physical = 131 ; free virtual = 1280
2default:defaulth px� 
�
synthesizing module '%s'638*oasys2
main2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
492default:default8@Z8-638h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2)
error_decoder_to_7seg2default:default2l
X/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/error_decoder_to_7seg.vhd2default:default2
342default:default2)
error_display_signals2default:default2)
error_decoder_to_7seg2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1112default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2)
error_decoder_to_7seg2default:default2n
X/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/error_decoder_to_7seg.vhd2default:default2
412default:default8@Z8-638h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
error_decoder_to_7seg2default:default2
12default:default2
12default:default2n
X/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/error_decoder_to_7seg.vhd2default:default2
412default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2

calculator2default:default2a
M/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/calculator.vhd2default:default2
352default:default2 
execute_calc2default:default2

calculator2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1172default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2

calculator2default:default2c
M/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/calculator.vhd2default:default2
432default:default8@Z8-638h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2#
complement_of_22default:default2f
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/complement_of_2.vhd2default:default2
352default:default2
C22default:default2#
complement_of_22default:default2c
M/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/calculator.vhd2default:default2
602default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2#
complement_of_22default:default2h
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/complement_of_2.vhd2default:default2
412default:default8@Z8-638h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2#
complement_of_12default:default2f
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/complement_of_1.vhd2default:default2
342default:default2
C12default:default2#
complement_of_12default:default2h
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/complement_of_2.vhd2default:default2
552default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2#
complement_of_12default:default2h
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/complement_of_1.vhd2default:default2
392default:default8@Z8-638h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2#
complement_of_12default:default2
22default:default2
12default:default2h
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/complement_of_1.vhd2default:default2
392default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2#
complement_of_22default:default2
32default:default2
12default:default2h
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/complement_of_2.vhd2default:default2
412default:default8@Z8-256h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2
A_comp_22default:default2c
M/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/calculator.vhd2default:default2
622default:default8@Z8-614h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2

calculator2default:default2
42default:default2
12default:default2c
M/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/calculator.vhd2default:default2
432default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2"
base_2_to_7seg2default:default2e
Q/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_2_to_7seg.vhd2default:default2
342default:default2
base_2_disp2default:default2"
base_2_to_7seg2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1242default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2"
base_2_to_7seg2default:default2g
Q/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_2_to_7seg.vhd2default:default2
422default:default8@Z8-638h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2"
base_2_to_7seg2default:default2
52default:default2
12default:default2g
Q/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_2_to_7seg.vhd2default:default2
422default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2"
base_8_to_7seg2default:default2e
Q/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_8_to_7seg.vhd2default:default2
342default:default2
base_8_disp2default:default2"
base_8_to_7seg2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1312default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2"
base_8_to_7seg2default:default2g
Q/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_8_to_7seg.vhd2default:default2
392default:default8@Z8-638h px� 
�
default block is never used226*oasys2g
Q/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_8_to_7seg.vhd2default:default2
452default:default8@Z8-226h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2"
base_8_to_7seg2default:default2
62default:default2
12default:default2g
Q/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_8_to_7seg.vhd2default:default2
392default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2#
base_10_to_7seg2default:default2f
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_10_to_7seg.vhd2default:default2
342default:default2 
base_10_disp2default:default2#
base_10_to_7seg2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1352default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2#
base_10_to_7seg2default:default2h
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_10_to_7seg.vhd2default:default2
392default:default8@Z8-638h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2#
base_10_to_7seg2default:default2
72default:default2
12default:default2h
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_10_to_7seg.vhd2default:default2
392default:default8@Z8-256h px� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2#
base_16_to_7seg2default:default2f
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_16_to_7seg.vhd2default:default2
342default:default2 
base_16_disp2default:default2#
base_16_to_7seg2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1392default:default8@Z8-3491h px� 
�
synthesizing module '%s'638*oasys2#
base_16_to_7seg2default:default2h
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_16_to_7seg.vhd2default:default2
392default:default8@Z8-638h px� 
�
default block is never used226*oasys2h
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_16_to_7seg.vhd2default:default2
452default:default8@Z8-226h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2#
base_16_to_7seg2default:default2
82default:default2
12default:default2h
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_16_to_7seg.vhd2default:default2
392default:default8@Z8-256h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2
	clk_count2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1442default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2$
sig_counter_disp2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1542default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2 
sig_overflow2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2$
sig_counter_disp2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2!
err_display_02default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2!
err_display_12default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2!
err_display_22default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2!
err_display_32default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2
DisplayMode2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2'
sig_base_10_display2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2'
sig_base_16_display2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2(
sig_base_2_display_02default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2(
sig_base_2_display_12default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2(
sig_base_2_display_22default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2(
sig_base_2_display_32default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
Esignal '%s' is read in the process but is not in the sensitivity list614*oasys2&
sig_base_8_display2default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
1602default:default8@Z8-614h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2
main2default:default2
92default:default2
12default:default2]
G/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/main.vhd2default:default2
492default:default8@Z8-256h px� 
�
!design %s has unconnected port %s3331*oasys2
main2default:default2!
OperationMode2default:defaultZ8-3331h px� 
v
!design %s has unconnected port %s3331*oasys2
main2default:default2
clk2default:defaultZ8-3331h px� 
�
%s*synth2�
�Finished RTL Elaboration : Time (s): cpu = 00:00:17 ; elapsed = 00:00:19 . Memory (MB): peak = 1289.832 ; gain = 115.520 ; free physical = 167 ; free virtual = 1293
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:18 ; elapsed = 00:00:19 . Memory (MB): peak = 1289.832 ; gain = 115.520 ; free physical = 166 ; free virtual = 1293
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
X
Loading part %s157*device2%
xc7a35ticpg236-1L2default:defaultZ21-403h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
�
Parsing XDC File [%s]
179*designutils2z
d/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/constrs_1/imports/13_Basys3_Files/Basys3_Master.xdc2default:default8Z20-179h px� 
�
No ports matched '%s'.
584*	planAhead2$
OperationMode[0]2default:default2z
d/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/constrs_1/imports/13_Basys3_Files/Basys3_Master.xdc2default:default2
492default:default8@Z12-584h px�
�
"'%s' expects at least one object.
55*common2 
set_property2default:default2z
d/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/constrs_1/imports/13_Basys3_Files/Basys3_Master.xdc2default:default2
492default:default8@Z17-55h px�
�
No ports matched '%s'.
584*	planAhead2$
OperationMode[0]2default:default2z
d/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/constrs_1/imports/13_Basys3_Files/Basys3_Master.xdc2default:default2
502default:default8@Z12-584h px�
�
"'%s' expects at least one object.
55*common2 
set_property2default:default2z
d/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/constrs_1/imports/13_Basys3_Files/Basys3_Master.xdc2default:default2
502default:default8@Z17-55h px�
�
Finished Parsing XDC File [%s]
178*designutils2z
d/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/constrs_1/imports/13_Basys3_Files/Basys3_Master.xdc2default:default8Z20-178h px� 
�
�Implementation specific constraints were found while reading constraint file [%s]. These constraints will be ignored for synthesis but will be used in implementation. Impacted constraints are listed in the file [%s].
233*project2x
d/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/constrs_1/imports/13_Basys3_Files/Basys3_Master.xdc2default:default2*
.Xil/main_propImpl.xdc2default:defaultZ1-236h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common24
 Constraint Validation Runtime : 2default:default2
00:00:00.012default:default2
00:00:00.012default:default2
1600.0512default:default2
0.0002default:default2
1192default:default2
10582default:defaultZ17-722h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Constraint Validation : Time (s): cpu = 00:00:49 ; elapsed = 00:01:15 . Memory (MB): peak = 1600.051 ; gain = 425.738 ; free physical = 184 ; free virtual = 1124
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Loading part: xc7a35ticpg236-1L
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:49 ; elapsed = 00:01:15 . Memory (MB): peak = 1600.051 ; gain = 425.738 ; free physical = 184 ; free virtual = 1124
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:00:49 ; elapsed = 00:01:15 . Memory (MB): peak = 1600.051 ; gain = 425.738 ; free physical = 186 ; free virtual = 1125
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2
adder2default:default2c
M/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/calculator.vhd2default:default2
642default:default8@Z8-5818h px� 
�
}HDL ADVISOR - The operator resource <%s> is shared. To prevent sharing consider applying a KEEP on the output of the operator4233*oasys2

multiplier2default:default2c
M/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/calculator.vhd2default:default2
642default:default8@Z8-5818h px� 
�
!inferring latch for variable '%s'327*oasys2
calc_reg2default:default2c
M/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/calculator.vhd2default:default2
1012default:default8@Z8-327h px� 
�
!inferring latch for variable '%s'327*oasys2
Over_reg2default:default2c
M/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/calculator.vhd2default:default2
1032default:default8@Z8-327h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:49 ; elapsed = 00:01:15 . Memory (MB): peak = 1600.051 ; gain = 425.738 ; free physical = 176 ; free virtual = 1116
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      4 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 1     
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 4     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  16 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  16 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  16 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Hierarchical RTL Component report 
2default:defaulth p
x
� 
9
%s
*synth2!
Module main 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
D
%s
*synth2,
Module complement_of_2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
?
%s
*synth2'
Module calculator 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      4 Bit       Adders := 1     
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 4     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  16 Input      5 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  16 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  16 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
C
%s
*synth2+
Module base_8_to_7seg 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   8 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2j
VPart Resources:
DSPs: 90 (col length:60)
BRAMs: 100 (col length: RAMB18 60 RAMB36 30)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
!design %s has unconnected port %s3331*oasys2
main2default:default2!
OperationMode2default:defaultZ8-3331h px� 
v
!design %s has unconnected port %s3331*oasys2
main2default:default2
clk2default:defaultZ8-3331h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:00:52 ; elapsed = 00:01:19 . Memory (MB): peak = 1600.051 ; gain = 425.738 ; free physical = 150 ; free virtual = 1092
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:01:05 ; elapsed = 00:01:37 . Memory (MB): peak = 1600.051 ; gain = 425.738 ; free physical = 110 ; free virtual = 967
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Timing Optimization : Time (s): cpu = 00:01:05 ; elapsed = 00:01:37 . Memory (MB): peak = 1600.051 ; gain = 425.738 ; free physical = 110 ; free virtual = 967
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
gCombinational loop found in the design within module (%s) during invertor optimization at instance (%s)4319*oasys2
main2default:default2
i_42default:default2h
R/home/fabio/vidado-projects/PED_ULA/PED_ULA.srcs/sources_1/new/base_10_to_7seg.vhd2default:default2
452default:default8@Z8-5971h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Technology Mapping : Time (s): cpu = 00:01:05 ; elapsed = 00:01:37 . Memory (MB): peak = 1608.059 ; gain = 433.746 ; free physical = 110 ; free virtual = 967
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished IO Insertion : Time (s): cpu = 00:01:06 ; elapsed = 00:01:39 . Memory (MB): peak = 1608.059 ; gain = 433.746 ; free physical = 108 ; free virtual = 965
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:01:06 ; elapsed = 00:01:39 . Memory (MB): peak = 1608.059 ; gain = 433.746 ; free physical = 108 ; free virtual = 965
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:01:06 ; elapsed = 00:01:39 . Memory (MB): peak = 1608.059 ; gain = 433.746 ; free physical = 108 ; free virtual = 965
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:01:06 ; elapsed = 00:01:39 . Memory (MB): peak = 1608.059 ; gain = 433.746 ; free physical = 108 ; free virtual = 965
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:01:06 ; elapsed = 00:01:39 . Memory (MB): peak = 1608.059 ; gain = 433.746 ; free physical = 108 ; free virtual = 965
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:01:06 ; elapsed = 00:01:39 . Memory (MB): peak = 1608.059 ; gain = 433.746 ; free physical = 108 ; free virtual = 965
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
| |BlackBox name |Instances |
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
D
%s*synth2,
|      |Cell   |Count |
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
D
%s*synth2,
|1     |CARRY4 |     2|
2default:defaulth px� 
D
%s*synth2,
|2     |LUT1   |     1|
2default:defaulth px� 
D
%s*synth2,
|3     |LUT2   |    11|
2default:defaulth px� 
D
%s*synth2,
|4     |LUT3   |    18|
2default:defaulth px� 
D
%s*synth2,
|5     |LUT4   |    12|
2default:defaulth px� 
D
%s*synth2,
|6     |LUT5   |    19|
2default:defaulth px� 
D
%s*synth2,
|7     |LUT6   |    58|
2default:defaulth px� 
D
%s*synth2,
|8     |MUXF7  |     3|
2default:defaulth px� 
D
%s*synth2,
|9     |LD     |     6|
2default:defaulth px� 
D
%s*synth2,
|10    |IBUF   |    14|
2default:defaulth px� 
D
%s*synth2,
|11    |OBUF   |    11|
2default:defaulth px� 
D
%s*synth2,
+------+-------+------+
2default:defaulth px� 
E
%s
*synth2-

Report Instance Areas: 
2default:defaulth p
x
� 
X
%s
*synth2@
,+------+---------------+-----------+------+
2default:defaulth p
x
� 
X
%s
*synth2@
,|      |Instance       |Module     |Cells |
2default:defaulth p
x
� 
X
%s
*synth2@
,+------+---------------+-----------+------+
2default:defaulth p
x
� 
X
%s
*synth2@
,|1     |top            |           |   155|
2default:defaulth p
x
� 
X
%s
*synth2@
,|2     |  execute_calc |calculator |    82|
2default:defaulth p
x
� 
X
%s
*synth2@
,+------+---------------+-----------+------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:01:06 ; elapsed = 00:01:39 . Memory (MB): peak = 1608.059 ; gain = 433.746 ; free physical = 108 ; free virtual = 965
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
r
%s
*synth2Z
FSynthesis finished with 0 errors, 1 critical warnings and 4 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Runtime : Time (s): cpu = 00:00:52 ; elapsed = 00:01:01 . Memory (MB): peak = 1608.059 ; gain = 123.527 ; free physical = 159 ; free virtual = 1016
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Complete : Time (s): cpu = 00:01:07 ; elapsed = 00:01:39 . Memory (MB): peak = 1608.066 ; gain = 433.746 ; free physical = 159 ; free virtual = 1017
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
f
-Analyzing %s Unisim elements for replacement
17*netlist2
252default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
�
!Unisim Transformation Summary:
%s111*project2Y
E  A total of 6 instances were transformed.
  LD => LDCE: 6 instances
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
422default:default2
252default:default2
32default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2"
synth_design: 2default:default2
00:01:112default:default2
00:01:432default:default2
1608.0662default:default2
447.1372default:default2
1892default:default2
10192default:defaultZ17-722h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2U
A/home/fabio/vidado-projects/PED_ULA/PED_ULA.runs/synth_1/main.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2r
^Executing : report_utilization -file main_utilization_synth.rpt -pb main_utilization_synth.pb
2default:defaulth px� 
�
�report_utilization: Time (s): cpu = 00:00:00.21 ; elapsed = 00:00:01 . Memory (MB): peak = 1632.070 ; gain = 0.000 ; free physical = 181 ; free virtual = 1017
*commonh px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Thu Jul  5 23:47:24 20182default:defaultZ17-206h px� 


End Record